


#Getting Started

Need to install the package use, edit database config and make migration

- Run command: pip install -r requirements.txt in the project root directory
- Go to 'providers-api/Test/settings.py' to edit database configurations
- Run command: python3 manage.py makemigrations api 
- Run command: python3 manage.py migrate api


#Development server
Run command python3 manage.py runserver. 
The server will start at this address "http://localhost:8000".
Now you can use API Documentation on this link "https://documenter.getpostman.com/view/5223484/S1ERuwds"

#Running unit tests
- Run command: python3 manage.py test api.test.model_tests
- Run command: python3 manage.py test api.test.polygon_view_tests
- Run command: python3 manage.py test api.test.provider_view_tests


