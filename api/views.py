from django.shortcuts import get_object_or_404
from rest_framework import generics
from api.serializers import ProvidersSerializer,PolygonSerializer
from api.models import Providers,Polygon
from django.http import JsonResponse
import json


#View for create a provider with POST request
#And get providers list with GET request

class GetOrCreateProviders(generics.ListCreateAPIView) :
    # query the database for all instances of the provider
    queryset = Providers.objects.all()
    serializer_class = ProvidersSerializer


#View for update a provider with PUT request
#Get one provider with GET request
#And delete one provider with DELETE request

class RetrieveOrUpdateOrDestroyProvider(generics.RetrieveUpdateDestroyAPIView):
    # query the database for all instances of the provider
    queryset = Providers.objects.all()
    serializer_class = ProvidersSerializer

    def get_object(self):
        return get_object_or_404(Providers,pk=self.kwargs['providers_id'])

#View for create a polygon with POST request
#And get polygon list with GET request

class GetOrCreatePolygon(generics.ListCreateAPIView):
    # query the database for all instances of the polygon
    queryset = Polygon.objects.all()
    serializer_class = PolygonSerializer


#View for update a polygon with PUT request
#Get one polygon with GET request
#And delete one polygon with DELETE request

class RetrieveOrUpdateOrDestroyPolygon(generics.RetrieveUpdateDestroyAPIView):
    # query the database for all instances of the polygon
    queryset = Polygon.objects.all()
    serializer_class = PolygonSerializer

    def get_object(self):
        return get_object_or_404(Polygon,pk=self.kwargs['polygon_id'])


#View for get the list of all polygon with the lat/lng pair send in the request

def getPolygonsByLatAndLng(request,lat,lng):
    # filter  the polygon instances in database for return a array
    polygon_list = [polygon for polygon in Polygon.objects.all() if str(json.loads(polygon.geo_json)['lat']) == str(lat) and str(json.loads(polygon.geo_json)['lng']) == str(lng)]
    return  JsonResponse(PolygonSerializer(polygon_list,many=True).data,safe=False)