# import the django rest framework serializers
from rest_framework import serializers
# import our models
from api import models

#Provider serializer
class ProvidersSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        # specify the model to use
        model = models.Providers
        # specify the field names we want to show in the api
        fields = ('id','name', 'phone_number','currency', 'email', 'language')

#Polygon serializer
class PolygonSerializer(serializers.HyperlinkedModelSerializer):

    provider = serializers.PrimaryKeyRelatedField(
    many=False,
    queryset= models.Providers.objects.all()
)
    provider = ProvidersSerializer(read_only=True)

    class Meta:
        # specify the model to use
        model = models.Polygon
        # specify the field names we want to show in the api
        fields = ('id','name', 'price','geo_json', 'provider')
