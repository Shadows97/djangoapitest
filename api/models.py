from django.db import models


# This class will more or less map to a table in the database
class Providers(models.Model):
    """ A Model for representing single line  in a Provider List. """
    # this defines a required text field of 10 characters.
    name = models.CharField(max_length=10)

    #this defines a required text field of 20 characters.
    phone_number = models.CharField(max_length=20)

    # this defines a required text field of 20 characters.
    currency = models.CharField(max_length=20)

    # this defines a required text field of 20 characters.
    language = models.CharField(max_length=20)

    # this defines a required email field.
    email = models.EmailField()



# This class will more or less map to a table in the database
class Polygon(models.Model):

    # this defines a required text field of 50 characters.
    name = models.CharField(max_length=50)

    # this defines a required float field .
    price = models.FloatField()

    # this defines a required text field of 255 characters.
    geo_json = models.CharField(max_length=255)

    # Define a foreign key relating this model to the provider model.
    # The parent will be able to access it's  with the related_name
    # 'polygon'. When a parent is deleted, this will be deleted as well.
    provider = models.ForeignKey(Providers, on_delete=models.CASCADE,related_name='polygon')

