import json
from django.test import TestCase
from django.test import Client

class ProviderTestCase(TestCase):
    """Tests for the provider view"""

    def setUp(self):
        """Initialize the TestCase with a test client"""
        self.client = Client()
        super(ProviderTestCase, self).setUp()

    def test_provider_post_delete(self):
        """
        Test the /provider/ POST method to add items
        and the /provider/ DELETE method to
        remove provider .
        """
        # define the data to post
        post_data = {
	        "name": "test1",
        "phone_number": "2345673",
        "currency": "cfa",
        "email": "tet@tet.com",
        "language": "fr"
            }

        # post a new provider
        response = self.client.post(
            '/v1/provider',
            json.dumps(post_data),
            content_type='application/json')
        print(response)
        # always check the response status code
        self.assertEqual(response.status_code, 201)

        # get the id of the saved object we posted
        polygon_id = response.data['id']

        # now try and delete it
        response = self.client.delete('/v1/provider/{0}'.format(polygon_id))

        # verify the response we got is a 204 no content
        self.assertEqual(response.status_code, 204)
