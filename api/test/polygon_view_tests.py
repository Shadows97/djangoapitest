import json
from django.test import TestCase
from django.test import Client
from api import models

class PolygonTestCase(TestCase):
    """Tests for the provider view"""

    def setUp(self):
        """Initialize the TestCase with a test client"""
        self.client = Client()
        self.test_list = models.Providers.objects.create(name="Provider1",email="test@test.com",phone_number="1234567",language="french",currency="EUR")
        super(PolygonTestCase, self).setUp()

    def test_polygon_post_delete(self):
        """
        Test the /polygon/ POST method to add items
        and the /polygon/ DELETE method to
        remove polygon .
        """
        # define the data to post
        post_data = {
	        "name":"test4",
            "price": "200",
	        "geo_json":"{\"lat\":3.0 , \"lng\":6.0}",
	        "provider": self.test_list.id
            }
        print(self.test_list.id)

        # post a new provider
        response = self.client.post(
            '/v1/polygon',
            json.dumps(post_data),
            content_type='application/json')
        print(response)
        # always check the response status code
        self.assertEqual(response.status_code, 201)

        # get the id of the saved object we posted
        polygon_id = response.data['id']

        # now try and delete it
        response = self.client.delete('/v1/polygon/{0}'.format(polygon_id))

        # verify the response we got is a 204 no content
        self.assertEqual(response.status_code, 204)
