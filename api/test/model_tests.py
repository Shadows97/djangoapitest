from django.test import TestCase
from api.models import Polygon,Providers


class ModelTestCase(TestCase):

    def test_provider(self):
        # create a provider object (this saves it to the database)
        Providers.objects.create(name="Provider1",email="test@test.com",phone_number="1234567",language="french",currency="EUR")
        # verify the object was saved
        provider = Providers.objects.get(name="Provider1")
        # verify the data is accurate and exists
        self.assertEqual(provider.name, "Provider1")
        # delete the provider
        provider.delete()
        # try to retrieve the provider and make sure it's not there
        try:
            retrieved = Providers.objects.get(name="Provider1")
        # catch the DoesNotExist error that we're hoping to get
        except Providers.DoesNotExist:
            retrieved = None
        self.assertEqual(retrieved, None)


    def test_polygon(self):
        """
           This test creates a provider, adds an polygon to it, and verifies
           that polygon is accessible through the 'provider' related name. Then,
           it deletes everything.
           """
        provider =Providers.objects.create(name="Provider1",email="test@test.com",phone_number="1234567",language="french",currency="EUR")

        polygon = Polygon.objects.create(
            provider=provider,
            name="Test Item",
            price=200,
            geo_json="{\"lat\":3.0 , \"lng\":6.0}"
        )

        # verify that the related name returns the todo_list item
        self.assertEqual(provider.polygon.count(), 1)
        self.assertEqual(provider.polygon.first(), polygon)

        # now delete the list. This should delete any items with it
        provider.delete()
        # verify the polygon was deleted with the provider
        # due to the CASCADE attribute we gave our model
        try:
            retrieved = Polygon.objects.get(name="Test Item")
        except Polygon.DoesNotExist:
            retrieved = None
        self.assertEqual(retrieved, None)